# Datenverarbeitung mit Python — Eine Einführung

Material zu meinem Vortag auf der DACH Meteorologen-Tagung am 21.03.2019 in Garmisch-Partenkirchen.

Dieses Repositorium enthällt:

1. die Präsentation (`1-talk.pdf`, `1-talk.html`) und das zugehörige Jupyter Notebook `1-talk.ipynb`
2. Beispieldaten unter `./data/`
    - `temperature.csv` — synthetische Stationsdaten der 2-Meter-Temperatur
    - `cosmo-d2_germany_rotated-lat-lon_single-level_2019020512_???_T_2M.nc` — COSMO-D2 Vorhersagen, heruntergeladen vom Open-Data-Server des DWD ([https://opendata.dwd.de](https://opendata.dwd.de/)).

Der Code, mit dem diese Daten erzeugt bzw. heruntergeladen wurden, ist im Notebook `2-data.ipynb` enthalten. Für die Vorhersagedaten des Deutschen Wetterdienstes gelten die Nutzungsbedingungen unter [https://www.dwd.de/copyright](https://www.dwd.de/copyright).

## Erste Schritte

1. Python installieren -> Anaconda ([https://www.anaconda.com/distribution/](https://www.anaconda.com/distribution/))
2. Zusätzliche Pakete aus diesem Tutorial installieren:
    - `conda install netcdf4`
    - `conda install -c conda-forge cartopy`
3. Jupyter Notebook starten: `jupyter notebook`
4. `01-talk.ipynb` öffnen


Eckhard Kadasch, 20. März 2019

